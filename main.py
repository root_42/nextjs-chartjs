from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI,UploadFile,File
import requests

from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*']
)

db = [{"name": "Aman","physics_marks":90,"maths_marks":90},
      {"name":"Kedar","physics_marks":94,"maths_marks":100}]

@app.get("/")
def get_Data():
    results = []
    for data in db:
        results.append({"name":data["name"],"physics_marks":data["physics_marks"],"maths_marks":data["maths_marks"]})
    return results

import pandas as pd
@app.get("/upload_csv")
def upload_csv():
    dataframe = pd.read_csv("./test.csv")
    return dataframe.to_json()