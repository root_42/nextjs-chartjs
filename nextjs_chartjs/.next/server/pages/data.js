module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/data.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/data.js":
/*!***********************!*\
  !*** ./pages/data.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return chartIt; });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ \"react-chartjs-2\");\n/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);\n\nvar _jsxFileName = \"/home/bhashkar/Desktop/Ayruz/nextjs-chartjs/nextjs_chartjs/pages/data.js\";\n\n\n/* export async function getStaticProps(context) {\n  const response = await fetch(\"http://127.0.0.1:8000\");\n  const data = await response.json();\n  return {\n    props: {\n      result_data: data\n    }\n  };\n}\n */\n\nasync function getData() {\n  let physics = [],\n      maths = [];\n  const response = await fetch(\"http://127.0.0.1:8000\");\n  const data = await response.json();\n  data.map(data => physics.push(data.physics_marks));\n  return physics;\n}\n\nasync function plot() {\n  let physics = await getData();\n  const data = {\n    labels: ['Red', 'Green', 'Yellow'],\n    datasets: [{\n      data: physics,\n      backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],\n      hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']\n    }]\n  };\n  return data;\n}\n\nasync function chartIt() {\n  let data;\n  return data = await plot(), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"div\", {\n    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"h2\", {\n      children: \" Chart\"\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 52,\n      columnNumber: 7\n    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__[\"Doughnut\"], {\n      data: data,\n      width: 200,\n      height: 80\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 54,\n      columnNumber: 7\n    }, this)]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 51,\n    columnNumber: 5\n  }, this);\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9kYXRhLmpzPzM0YjkiXSwibmFtZXMiOlsiZ2V0RGF0YSIsInBoeXNpY3MiLCJtYXRocyIsInJlc3BvbnNlIiwiZmV0Y2giLCJkYXRhIiwianNvbiIsIm1hcCIsInB1c2giLCJwaHlzaWNzX21hcmtzIiwicGxvdCIsImxhYmVscyIsImRhdGFzZXRzIiwiYmFja2dyb3VuZENvbG9yIiwiaG92ZXJCYWNrZ3JvdW5kQ29sb3IiLCJjaGFydEl0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLGVBQWVBLE9BQWYsR0FBeUI7QUFDdkIsTUFBSUMsT0FBTyxHQUFHLEVBQWQ7QUFBQSxNQUFrQkMsS0FBSyxHQUFHLEVBQTFCO0FBQ0EsUUFBTUMsUUFBUSxHQUFHLE1BQU1DLEtBQUssQ0FBQyx1QkFBRCxDQUE1QjtBQUNBLFFBQU1DLElBQUksR0FBRyxNQUFNRixRQUFRLENBQUNHLElBQVQsRUFBbkI7QUFDQUQsTUFBSSxDQUFDRSxHQUFMLENBQVVGLElBQUQsSUFBVUosT0FBTyxDQUFDTyxJQUFSLENBQWFILElBQUksQ0FBQ0ksYUFBbEIsQ0FBbkI7QUFDQSxTQUFPUixPQUFQO0FBQ0Q7O0FBQ0QsZUFBZVMsSUFBZixHQUFzQjtBQUNwQixNQUFJVCxPQUFPLEdBQUcsTUFBTUQsT0FBTyxFQUEzQjtBQUNBLFFBQU1LLElBQUksR0FBRztBQUNYTSxVQUFNLEVBQUUsQ0FDTixLQURNLEVBRU4sT0FGTSxFQUdOLFFBSE0sQ0FERztBQU1YQyxZQUFRLEVBQUUsQ0FBQztBQUNUUCxVQUFJLEVBQUVKLE9BREc7QUFFVFkscUJBQWUsRUFBRSxDQUNmLFNBRGUsRUFFZixTQUZlLEVBR2YsU0FIZSxDQUZSO0FBT1RDLDBCQUFvQixFQUFFLENBQ3BCLFNBRG9CLEVBRXBCLFNBRm9CLEVBR3BCLFNBSG9CO0FBUGIsS0FBRDtBQU5DLEdBQWI7QUFxQkEsU0FBT1QsSUFBUDtBQUNEOztBQUdjLGVBQWVVLE9BQWYsR0FBeUI7QUFDdEMsTUFBSVYsSUFBSjtBQUNBLFNBQ0VBLElBQUksR0FBRyxNQUFNSyxJQUFJLEVBQWpCLGVBQ0E7QUFBQSw0QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBR0UscUVBQUMsd0RBQUQ7QUFBVSxVQUFJLEVBQUVMLElBQWhCO0FBQXNCLFdBQUssRUFBRSxHQUE3QjtBQUFrQyxZQUFNLEVBQUU7QUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUZGO0FBUUQiLCJmaWxlIjoiLi9wYWdlcy9kYXRhLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IERvdWdobnV0IH0gZnJvbSAncmVhY3QtY2hhcnRqcy0yJztcbi8qIGV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQcm9wcyhjb250ZXh0KSB7XG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZmV0Y2goXCJodHRwOi8vMTI3LjAuMC4xOjgwMDBcIik7XG4gIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwb25zZS5qc29uKCk7XG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIHJlc3VsdF9kYXRhOiBkYXRhXG4gICAgfVxuICB9O1xufVxuICovXG5hc3luYyBmdW5jdGlvbiBnZXREYXRhKCkge1xuICBsZXQgcGh5c2ljcyA9IFtdLCBtYXRocyA9IFtdXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZmV0Y2goXCJodHRwOi8vMTI3LjAuMC4xOjgwMDBcIik7XG4gIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwb25zZS5qc29uKCk7XG4gIGRhdGEubWFwKChkYXRhKSA9PiBwaHlzaWNzLnB1c2goZGF0YS5waHlzaWNzX21hcmtzKSlcbiAgcmV0dXJuIHBoeXNpY3M7XG59XG5hc3luYyBmdW5jdGlvbiBwbG90KCkge1xuICBsZXQgcGh5c2ljcyA9IGF3YWl0IGdldERhdGEoKTtcbiAgY29uc3QgZGF0YSA9IHtcbiAgICBsYWJlbHM6IFtcbiAgICAgICdSZWQnLFxuICAgICAgJ0dyZWVuJyxcbiAgICAgICdZZWxsb3cnXG4gICAgXSxcbiAgICBkYXRhc2V0czogW3tcbiAgICAgIGRhdGE6IHBoeXNpY3MsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IFtcbiAgICAgICAgJyNGRjYzODQnLFxuICAgICAgICAnIzM2QTJFQicsXG4gICAgICAgICcjRkZDRTU2J1xuICAgICAgXSxcbiAgICAgIGhvdmVyQmFja2dyb3VuZENvbG9yOiBbXG4gICAgICAgICcjRkY2Mzg0JyxcbiAgICAgICAgJyMzNkEyRUInLFxuICAgICAgICAnI0ZGQ0U1NidcbiAgICAgIF1cbiAgICB9XVxuXG4gIH07XG4gIHJldHVybiBkYXRhO1xufVxuXG5cbmV4cG9ydCBkZWZhdWx0IGFzeW5jIGZ1bmN0aW9uIGNoYXJ0SXQoKSB7XG4gIGxldCBkYXRhO1xuICByZXR1cm4gKFxuICAgIGRhdGEgPSBhd2FpdCBwbG90KCksXG4gICAgPGRpdj5cbiAgICAgIDxoMj4gQ2hhcnRcbiAgICA8L2gyPlxuICAgICAgPERvdWdobnV0IGRhdGE9e2RhdGF9IHdpZHRoPXsyMDB9IGhlaWdodD17ODB9IC8+XG4gICAgPC9kaXY+XG4gIClcbn1cblxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/data.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiPzU4OGUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoicmVhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react\n");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-chartjs-2\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1jaGFydGpzLTJcIj84MDlkIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0LWNoYXJ0anMtMi5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWNoYXJ0anMtMlwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react-chartjs-2\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react/jsx-dev-runtime\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIj9jZDkwIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0L2pzeC1kZXYtcnVudGltZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react/jsx-dev-runtime\n");

/***/ })

/******/ });