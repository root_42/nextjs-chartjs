module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/line_graph.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/line_graph.js":
/*!*****************************!*\
  !*** ./pages/line_graph.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return LineChart; });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ \"react-chartjs-2\");\n/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);\n\nvar _jsxFileName = \"/home/bhashkar/Desktop/Ayruz/nextjs-chartjs/nextjs_chartjs/pages/line_graph.js\";\n\n\n\n/*#__PURE__*/\nObject(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"canvas\", {\n  id: \"chart\",\n  height: \"20\",\n  width: \"80\"\n}, void 0, false, {\n  fileName: _jsxFileName,\n  lineNumber: 3,\n  columnNumber: 1\n}, undefined);\n\nasync function chartIt() {\n  const data = await getData();\n  const myChart = new Chart({\n    type: 'line',\n    data: {\n      labels: data.xlabels,\n      datasets: [{\n        label: 'Combined Land-Surface Air and Sea surface water Temperature in C',\n        data: data.ytemps,\n        fill: false,\n        backgroundColor: 'rgba(255, 99, 132, 0.2)',\n        borderColor: 'rgba(255, 99, 132, 1)',\n        borderWidth: 1\n      }]\n    },\n    options: {\n      responsive: true,\n      maintainAspectRation: false\n    }\n  });\n}\n\nconsole.log(getData());\n\nasync function getData() {\n  const xlabels = [];\n  const ytemps = [];\n  const response = await fetch(\"/home/bhashkar/Desktop/Ayruz/internship_iiitmk/nextjs/fastapi/nextjs_chartjs/pages/ZonAnn.Ts+dSST.csv\");\n  const data = await response.text();\n  const table = data.split(\"\\n\").slice(1);\n  table.forEach(elt => {\n    const columns = elt.split(\",\");\n    const year = columns[0];\n    xlabels.push(year);\n    const temp = columns[1];\n    ytemps.push(parseFloat(temp) + 14);\n    console.log(year, temp);\n  });\n  return {\n    xlabels,\n    ytemps\n  };\n}\n\nfunction LineChart() {\n  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"div\", {\n    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"h2\", {\n      children: \"Line Example\"\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 55,\n      columnNumber: 7\n    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__[\"Line\"], {\n      data: chartIt(),\n      width: 400,\n      height: 400\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 56,\n      columnNumber: 7\n    }, this)]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 54,\n    columnNumber: 5\n  }, this);\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9saW5lX2dyYXBoLmpzPzQyOGIiXSwibmFtZXMiOlsiY2hhcnRJdCIsImRhdGEiLCJnZXREYXRhIiwibXlDaGFydCIsIkNoYXJ0IiwidHlwZSIsImxhYmVscyIsInhsYWJlbHMiLCJkYXRhc2V0cyIsImxhYmVsIiwieXRlbXBzIiwiZmlsbCIsImJhY2tncm91bmRDb2xvciIsImJvcmRlckNvbG9yIiwiYm9yZGVyV2lkdGgiLCJvcHRpb25zIiwicmVzcG9uc2l2ZSIsIm1haW50YWluQXNwZWN0UmF0aW9uIiwiY29uc29sZSIsImxvZyIsInJlc3BvbnNlIiwiZmV0Y2giLCJ0ZXh0IiwidGFibGUiLCJzcGxpdCIsInNsaWNlIiwiZm9yRWFjaCIsImVsdCIsImNvbHVtbnMiLCJ5ZWFyIiwicHVzaCIsInRlbXAiLCJwYXJzZUZsb2F0IiwiTGluZUNoYXJ0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFDQTs7QUFDQTtBQUFBO0FBQVEsSUFBRSxFQUFDLE9BQVg7QUFBbUIsUUFBTSxFQUFDLElBQTFCO0FBQStCLE9BQUssRUFBQztBQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUNBLGVBQWVBLE9BQWYsR0FBeUI7QUFDckIsUUFBTUMsSUFBSSxHQUFHLE1BQU1DLE9BQU8sRUFBMUI7QUFFQSxRQUFNQyxPQUFPLEdBQUcsSUFBSUMsS0FBSixDQUFXO0FBQ3ZCQyxRQUFJLEVBQUUsTUFEaUI7QUFFdkJKLFFBQUksRUFBRTtBQUNGSyxZQUFNLEVBQUVMLElBQUksQ0FBQ00sT0FEWDtBQUVGQyxjQUFRLEVBQUUsQ0FBQztBQUNQQyxhQUFLLEVBQUUsa0VBREE7QUFFUFIsWUFBSSxFQUFFQSxJQUFJLENBQUNTLE1BRko7QUFHUEMsWUFBSSxFQUFFLEtBSEM7QUFJUEMsdUJBQWUsRUFDWCx5QkFMRztBQU1QQyxtQkFBVyxFQUNQLHVCQVBHO0FBUVBDLG1CQUFXLEVBQUU7QUFSTixPQUFEO0FBRlIsS0FGaUI7QUFldkJDLFdBQU8sRUFBRTtBQUNMQyxnQkFBVSxFQUFFLElBRFA7QUFFTEMsMEJBQW9CLEVBQUU7QUFGakI7QUFmYyxHQUFYLENBQWhCO0FBcUJIOztBQUVEQyxPQUFPLENBQUNDLEdBQVIsQ0FBWWpCLE9BQU8sRUFBbkI7O0FBRUEsZUFBZUEsT0FBZixHQUF5QjtBQUVyQixRQUFNSyxPQUFPLEdBQUcsRUFBaEI7QUFDQSxRQUFNRyxNQUFNLEdBQUcsRUFBZjtBQUNBLFFBQU1VLFFBQVEsR0FBRyxNQUFNQyxLQUFLLENBQUMsdUdBQUQsQ0FBNUI7QUFDQSxRQUFNcEIsSUFBSSxHQUFHLE1BQU1tQixRQUFRLENBQUNFLElBQVQsRUFBbkI7QUFDQSxRQUFNQyxLQUFLLEdBQUd0QixJQUFJLENBQUN1QixLQUFMLENBQVcsSUFBWCxFQUFpQkMsS0FBakIsQ0FBdUIsQ0FBdkIsQ0FBZDtBQUNBRixPQUFLLENBQUNHLE9BQU4sQ0FBY0MsR0FBRyxJQUFJO0FBQ2pCLFVBQU1DLE9BQU8sR0FBR0QsR0FBRyxDQUFDSCxLQUFKLENBQVUsR0FBVixDQUFoQjtBQUNBLFVBQU1LLElBQUksR0FBR0QsT0FBTyxDQUFDLENBQUQsQ0FBcEI7QUFDQXJCLFdBQU8sQ0FBQ3VCLElBQVIsQ0FBYUQsSUFBYjtBQUNBLFVBQU1FLElBQUksR0FBR0gsT0FBTyxDQUFDLENBQUQsQ0FBcEI7QUFDQWxCLFVBQU0sQ0FBQ29CLElBQVAsQ0FBWUUsVUFBVSxDQUFDRCxJQUFELENBQVYsR0FBbUIsRUFBL0I7QUFDQWIsV0FBTyxDQUFDQyxHQUFSLENBQVlVLElBQVosRUFBa0JFLElBQWxCO0FBRUgsR0FSRDtBQVNBLFNBQU87QUFBRXhCLFdBQUY7QUFBV0c7QUFBWCxHQUFQO0FBR0g7O0FBQ2MsU0FBU3VCLFNBQVQsR0FBcUI7QUFDaEMsc0JBQ0E7QUFBQSw0QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBRUUscUVBQUMsb0RBQUQ7QUFDRSxVQUFJLEVBQUVqQyxPQUFPLEVBRGY7QUFFRSxXQUFLLEVBQUUsR0FGVDtBQUdFLFlBQU0sRUFBRTtBQUhWO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFEQTtBQVVIIiwiZmlsZSI6Ii4vcGFnZXMvbGluZV9ncmFwaC5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCx7Q29tcG9uZW50fSBmcm9tICdyZWFjdCdcbmltcG9ydCB7TGluZX0gZnJvbSAncmVhY3QtY2hhcnRqcy0yJ1xuPGNhbnZhcyBpZD1cImNoYXJ0XCIgaGVpZ2h0PVwiMjBcIiB3aWR0aD1cIjgwXCI+PC9jYW52YXM+XG5hc3luYyBmdW5jdGlvbiBjaGFydEl0KCkge1xuICAgIGNvbnN0IGRhdGEgPSBhd2FpdCBnZXREYXRhKCk7XG5cbiAgICBjb25zdCBteUNoYXJ0ID0gbmV3IENoYXJ0KCB7XG4gICAgICAgIHR5cGU6ICdsaW5lJyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgbGFiZWxzOiBkYXRhLnhsYWJlbHMsXG4gICAgICAgICAgICBkYXRhc2V0czogW3tcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NvbWJpbmVkIExhbmQtU3VyZmFjZSBBaXIgYW5kIFNlYSBzdXJmYWNlIHdhdGVyIFRlbXBlcmF0dXJlIGluIEMnLFxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEueXRlbXBzLFxuICAgICAgICAgICAgICAgIGZpbGw6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjpcbiAgICAgICAgICAgICAgICAgICAgJ3JnYmEoMjU1LCA5OSwgMTMyLCAwLjIpJyxcbiAgICAgICAgICAgICAgICBib3JkZXJDb2xvcjpcbiAgICAgICAgICAgICAgICAgICAgJ3JnYmEoMjU1LCA5OSwgMTMyLCAxKScsXG4gICAgICAgICAgICAgICAgYm9yZGVyV2lkdGg6IDFcbiAgICAgICAgICAgIH1dXG4gICAgICAgIH0sXG4gICAgICAgIG9wdGlvbnM6IHtcbiAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgICAgICBtYWludGFpbkFzcGVjdFJhdGlvbjogZmFsc2VcbiAgICAgICAgfVxuICAgIH0pO1xuXG59XG5cbmNvbnNvbGUubG9nKGdldERhdGEoKSk7XG5cbmFzeW5jIGZ1bmN0aW9uIGdldERhdGEoKSB7XG5cbiAgICBjb25zdCB4bGFiZWxzID0gW107XG4gICAgY29uc3QgeXRlbXBzID0gW107XG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaChcIi9ob21lL2JoYXNoa2FyL0Rlc2t0b3AvQXlydXovaW50ZXJuc2hpcF9paWl0bWsvbmV4dGpzL2Zhc3RhcGkvbmV4dGpzX2NoYXJ0anMvcGFnZXMvWm9uQW5uLlRzK2RTU1QuY3N2XCIpO1xuICAgIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwb25zZS50ZXh0KCk7XG4gICAgY29uc3QgdGFibGUgPSBkYXRhLnNwbGl0KFwiXFxuXCIpLnNsaWNlKDEpO1xuICAgIHRhYmxlLmZvckVhY2goZWx0ID0+IHtcbiAgICAgICAgY29uc3QgY29sdW1ucyA9IGVsdC5zcGxpdChcIixcIik7XG4gICAgICAgIGNvbnN0IHllYXIgPSBjb2x1bW5zWzBdO1xuICAgICAgICB4bGFiZWxzLnB1c2goeWVhcik7XG4gICAgICAgIGNvbnN0IHRlbXAgPSBjb2x1bW5zWzFdO1xuICAgICAgICB5dGVtcHMucHVzaChwYXJzZUZsb2F0KHRlbXApICsgMTQpXG4gICAgICAgIGNvbnNvbGUubG9nKHllYXIsIHRlbXApO1xuXG4gICAgfSk7XG4gICAgcmV0dXJuIHsgeGxhYmVscywgeXRlbXBzIH07XG5cblxufVxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gTGluZUNoYXJ0ICgpeyBcbiAgICByZXR1cm4gKFxuICAgIDxkaXY+XG4gICAgICA8aDI+TGluZSBFeGFtcGxlPC9oMj5cbiAgICAgIDxMaW5lXG4gICAgICAgIGRhdGE9e2NoYXJ0SXQoKX1cbiAgICAgICAgd2lkdGg9ezQwMH1cbiAgICAgICAgaGVpZ2h0PXs0MDB9XG4gICAgICAvPlxuICAgIDwvZGl2PlxuICApO1xufSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/line_graph.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiPzU4OGUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoicmVhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react\n");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-chartjs-2\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1jaGFydGpzLTJcIj84MDlkIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0LWNoYXJ0anMtMi5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWNoYXJ0anMtMlwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react-chartjs-2\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react/jsx-dev-runtime\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIj9jZDkwIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0L2pzeC1kZXYtcnVudGltZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react/jsx-dev-runtime\n");

/***/ })

/******/ });