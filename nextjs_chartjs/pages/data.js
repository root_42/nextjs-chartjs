import React from 'react';
import { Doughnut } from 'react-chartjs-2';
/* export async function getStaticProps(context) {
  const response = await fetch("http://127.0.0.1:8000");
  const data = await response.json();
  return {
    props: {
      result_data: data
    }
  };
}
 */
async function getData() {
  let physics = [], maths = []
  const response = await fetch("http://127.0.0.1:8000");
  const data = await response.json();
  data.map((data) => physics.push(data.physics_marks))
  return physics;
}
async function plot() {
  let physics = await getData();
  const data = {
    labels: [
      'Red',
      'Green',
      'Yellow'
    ],
    datasets: [{
      data: physics,
      backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56'
      ],
      hoverBackgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56'
      ]
    }]

  };
  return data;
}


export default async function chartIt() {
  let data;
  return (
    data = await plot(),
    <div>
      <h2> Chart
    </h2>
      <Doughnut data={data} width={200} height={80} />
    </div>
  )
}

