import React,{Component} from 'react'
import {Line} from 'react-chartjs-2'
<canvas id="chart" height="20" width="80"></canvas>
async function chartIt() {
    const data = await getData();

    const myChart = new Chart( {
        type: 'line',
        data: {
            labels: data.xlabels,
            datasets: [{
                label: 'Combined Land-Surface Air and Sea surface water Temperature in C',
                data: data.ytemps,
                fill: false,
                backgroundColor:
                    'rgba(255, 99, 132, 0.2)',
                borderColor:
                    'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRation: false
        }
    });

}

console.log(getData());

async function getData() {

    const xlabels = [];
    const ytemps = [];
    const response = await fetch("/home/bhashkar/Desktop/Ayruz/internship_iiitmk/nextjs/fastapi/nextjs_chartjs/pages/ZonAnn.Ts+dSST.csv");
    const data = await response.text();
    const table = data.split("\n").slice(1);
    table.forEach(elt => {
        const columns = elt.split(",");
        const year = columns[0];
        xlabels.push(year);
        const temp = columns[1];
        ytemps.push(parseFloat(temp) + 14)
        console.log(year, temp);

    });
    return { xlabels, ytemps };


}
export default function LineChart (){ 
    return (
    <div>
      <h2>Line Example</h2>
      <Line
        data={chartIt()}
        width={400}
        height={400}
      />
    </div>
  );
}